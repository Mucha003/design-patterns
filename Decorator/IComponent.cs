﻿namespace Decorator
{
    // Utilisation de cet Interface pour la Decoration.
    public interface IComponent
    {
        double GetPrix();
        string Functionne();
    }
}
