﻿namespace Decorator
{
    using System;

    public class SonDecor : IComponent
    {
        private IComponent _decorA;

        public SonDecor(IComponent pDecor)
        {
            this._decorA = pDecor;
        }

        public override string ToString() => $"{this._decorA.ToString()} \r\nRadio 350XZ";


        public string Functionne() => $"{this._decorA.Functionne()}, Radio Allumé ";

        public double GetPrix() => this._decorA.GetPrix() + 300;
    }
}
