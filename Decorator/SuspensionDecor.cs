﻿namespace Decorator
{
    using System;

    public class SuspensionDecor : IComponent
    {
        private IComponent _decorA;

        public SuspensionDecor(IComponent pComponent)
        {
            this._decorA = pComponent;
        }

        public string Functionne() => $"{this._decorA.Functionne()}, Haute suspension";

        public double GetPrix() => this._decorA.GetPrix() + 1200;

        public override string ToString() => $"Susmpension Haut \r\n {this._decorA.ToString()}";
    }
}
