﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creation du Component Principal avec le Type IComponent
            // pour permettre la decoration.
            IComponent monAuto = new Auto("2018", "4 portes", 20000);
            Console.WriteLine(monAuto);

            // Si on a besoin d'utiliser une Methode propre a Auto.
            // Nous devons le caster(Auto).
            ((Auto)monAuto).Portes(true);

            Console.WriteLine();
            Console.WriteLine("------------------------");
            Console.WriteLine();
            Console.WriteLine("*********    Decorateur Son    *********");
            Console.WriteLine();


            // Important
            // Nous Alons decorer avec un system de Son.
            monAuto = new SonDecor(monAuto);

            // Verification des Ajjouts du decorator Son.
            Console.WriteLine(monAuto);
            Console.WriteLine($"Prix : {monAuto.GetPrix()}");
            Console.WriteLine(monAuto.Functionne());
            Console.WriteLine();

            Console.WriteLine("------------------------");
            Console.WriteLine();
            Console.WriteLine("*********    Decorateur Nitro    *********");
            Console.WriteLine();

            monAuto = new NitroDecor(monAuto);
            Console.WriteLine(monAuto);
            Console.WriteLine($"Prix : {monAuto.GetPrix()}");
            Console.WriteLine(monAuto.Functionne());

            // pour utiliser une Methode secifique d'un Decorateur 
            // Faut la caster
            ((NitroDecor)monAuto).UseNitro();

            // Mais si par exemple Nous le docarant avec un autre
            // Et ensuite on veuille utiliser le decorateur precedent
            // Il va nous envoyer une erreur.

            Console.WriteLine();



            Console.ReadKey();
        }
    }
}
