﻿namespace Decorator
{
    using System;

    public class NitroDecor : IComponent
    {
        // IMPORTANT
        // Variable de reference vers qui on va decorer.
        // ce avec cette var qu'on va c communiquer vers qui on va decorer(Auto)
        private IComponent _decorA;

        // On lui passe l'objet qui va etre Decore.
        public NitroDecor(IComponent pCompo)
        {
            this._decorA = pCompo;
        }

        public override string ToString() => $"{this._decorA.ToString()} \r\nSystem de Nitro";

        public void UseNitro() => Console.WriteLine("Nitro En utilisation");

        public string Functionne() => $"{this._decorA.Functionne()}, Nitro Ajouté";

        public double GetPrix() => this._decorA.GetPrix() + 40000;
    }
}
