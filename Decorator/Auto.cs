﻿namespace Decorator
{
    using System;

    // Component
    public class Auto : IComponent
    {
        private string _model;
        private string _carecteristique;
        private double _prix;

        public Auto(string pModel, string pCaract, double pCout)
        {
            this._model = pModel;
            this._prix = pCout;
            this._carecteristique = pCaract;
        }

        public void Portes(bool pEtat)
        {
            if (pEtat)
                Console.WriteLine("Portes Fermes");
            else
                Console.WriteLine("Portes ouvertes");
        }

        public override string ToString() => $"Model {_model}, {_carecteristique} \r\n";


        // Methodes IComponent
        public double GetPrix() => this._prix;

        public string Functionne() => "Motor Alluméé";
    }
}
