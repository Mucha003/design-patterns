using System;

namespace Strategie
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = string.Empty;
            string options = string.Empty;
            string symbol = string.Empty;

            double valeur1 = 0;
            double valeur2 = 0;
            double Result = 0;

            
            // Notre VAR de reff aux algorithm
            // Instancier valeur par default
            IOperarion myOperation = new Addition();


            while (options != "5")
            {
                Console.WriteLine("1- Addition, 2- Soustraction, 3- Multi, 4- Div, 5- Sortir");
                options = Console.ReadLine();

                if (options == "5")
                    break;

                Console.WriteLine("Donne moi le premier chiffre");
                data = Console.ReadLine();
                valeur1 = Convert.ToDouble(data);

                Console.WriteLine("Donne moi le second chiffre");
                data = Console.ReadLine();
                valeur2 = Convert.ToDouble(data);

                // Ici on selectionne l'Algorithm seleon son Besoin
                switch (options)
                {
                    case "1":
                        myOperation = new Addition();
                        symbol = "+";
                        break;

                    case "2":
                        myOperation = new Soustraction();
                        symbol = "-";
                        break;

                    case "3":
                        myOperation = new Multiplication();
                        symbol = "*";
                        break;

                    case "4":
                        myOperation = new Division();
                        symbol = "/";
                        break;
                }

                Result = myOperation.Operation(valeur1, valeur2);
                Console.WriteLine($"Le resultat de : {valeur2} {symbol} {valeur2}  = {Result}");
            }

            Console.ReadKey();
        }
    }
}
