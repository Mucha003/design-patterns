﻿namespace Strategie
{
    public class Soustraction : IOperarion
    {
        public double Operation(double numA, double numB)
        {
            return numA - numB;
        }
    }
}
