﻿namespace Strategie
{
    public class Multiplication : IOperarion
    {
        public double Operation(double numA, double numB)
        {
            return numA * numB;
        }
    }
}
